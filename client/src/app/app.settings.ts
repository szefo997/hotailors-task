import {isDevMode} from '@angular/core';
import {environment as dev} from '../environments/environment';
import {environment as prod} from '../environments/environment.prod';

export class AppSettings {

  public static readonly API_ENDPOINT = isDevMode() ? dev.config.host : prod.config.host;

}
