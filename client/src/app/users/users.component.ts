import {Component, OnDestroy, OnInit} from '@angular/core';
import {Select, Store} from "@ngxs/store";
import {Observable, Subject, Subscription} from "rxjs";
import {UsersChangeCondition, UsersFetch, UsersFilterName, UsersFilterSurname, UsersState} from "./users.state";
import {User} from "./models/user";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";

@Component({
  selector: 'rb-users',
  template: `
      <nb-layout center>
          <nb-layout-column>
              <nb-card [nbSpinner]="loading$ | async" nbSpinnerStatus="danger">
                  <nb-card-body>

                      <div class="row">
                          <div class="col-5">
                              <input #nameInput class="input-full-width size-medium shape-rectangle"
                                     (input)="onInputName(nameInput.value)"
                                     placeholder="Search by name..."
                                     nbInput
                                     type="text">
                          </div>
                          <div class="col-2">
                              <nb-radio-group [(value)]="selectedOption" (valueChange)="onSelectedOptionChange()">
                                  <nb-radio value="AND">AND</nb-radio>
                                  <nb-radio value="OR">OR</nb-radio>
                              </nb-radio-group>
                          </div>
                          <div class="col-5">
                              <input #surnameInput class="input-full-width size-medium shape-rectangle"
                                     (input)="onInputSurname(surnameInput.value)"
                                     placeholder="Search by a surname..."
                                     nbInput
                                     type="text">
                          </div>
                      </div>

                      <nb-list>
                          <nb-list-item *ngFor="let user of users$ | async">
                              <nb-user [name]="user.name" [title]="user.surname">
                              </nb-user>
                          </nb-list-item>
                      </nb-list>

                  </nb-card-body>
              </nb-card>

          </nb-layout-column>
      </nb-layout>
  `,
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {

  @Select(UsersState.users) public users$: Observable<User[]>;
  @Select(UsersState.loading) public loading$: Observable<boolean>;

  public selectedOption: 'AND' | 'OR' = 'AND';

  private _nameSubj = new Subject<string>();
  private _surnameSubj = new Subject<string>();
  private _nameSub: Subscription;
  private _surnameSub: Subscription;

  constructor(private _store: Store) {
  }

  ngOnInit() {
    this._store.dispatch(new UsersFetch());

    this._nameSub = this._nameSubj
      .pipe(
        debounceTime(250),
        distinctUntilChanged()
      )
      .subscribe(value => {
        this._store.dispatch(new UsersFilterName(value, this.selectedOption));
      });

    this._surnameSub = this._surnameSubj
      .pipe(
        debounceTime(250),
        distinctUntilChanged()
      )
      .subscribe(value => {
        this._store.dispatch(new UsersFilterSurname(value, this.selectedOption));
      });
  }

  ngOnDestroy(): void {
    this._nameSub.unsubscribe();
    this._surnameSub.unsubscribe();
  }

  public onSelectedOptionChange(): void {
    console.log(`onSelectedOptionChange condition: ${this.selectedOption}`);
    this._store.dispatch(new UsersChangeCondition(this.selectedOption));
  }

  public onInputName(name: string): void {
    console.log(`onKeydownName name: ${name}`);
    this._nameSubj.next(name);
  }

  public onInputSurname(surname: string): void {
    console.log(`onKeydownSurname surname: ${surname}`);
    this._surnameSubj.next(surname);
  }

}
