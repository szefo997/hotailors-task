import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, tap} from 'rxjs/operators';
import {Observable, of, throwError} from 'rxjs';
import {UsersService} from "./services/users.service";
import {User} from "./models/user";
import {Router} from "@angular/router";
import {NgZone} from "@angular/core";

export class UsersFetch {
  static readonly type = '[Users] fetch';

  constructor() {
  }
}

export class UsersFilterSurname {
  static readonly type = '[Users] filter surname';

  constructor(public surname: string, public condition: 'OR' | 'AND') {
  }
}

export class UsersFilterName {
  static readonly type = '[Users] filter name';

  constructor(public name: string, public condition: 'OR' | 'AND') {
  }
}

export class UsersChangeCondition {
  static readonly type = '[Users] change condition';

  constructor(public condition: 'OR' | 'AND') {
  }
}

export interface AuthStateModel {
  loading: boolean;
  name: string;
  surname: string;
  users: User[];
  dbUsers: User[];
}

@State<AuthStateModel>({
  name: 'users',
  defaults: {
    loading: false,
    name: '',
    surname: '',
    users: [],
    dbUsers: []
  }
})
export class UsersState {

  constructor(
    private _usersService: UsersService,
    private _ngZone: NgZone,
    private _router: Router
  ) {
  }

  @Selector()
  static loading(state: AuthStateModel): boolean {
    return state.loading;
  }

  @Selector()
  static users(state: AuthStateModel): User[] {
    return state.users;
  }

  @Action(UsersFetch)
  fetch({patchState}: StateContext<AuthStateModel>): Observable<any> {
    patchState({loading: true});
    return this._usersService.getUsers()
      .pipe(
        tap(users => {
          patchState({users, dbUsers: users})
        }),
        finalize(() => {
          patchState({loading: false});
        }),
        catchError(err => {
          if (err.status === 401) {
            this._ngZone.run(() => {
              this._router.navigateByUrl('login');
            });
          }
          return throwError(err);
        })
      )
  }

  @Action(UsersChangeCondition)
  changeCondition({patchState, getState}: StateContext<AuthStateModel>, {condition}: UsersChangeCondition): Observable<User[]> {
    const {surname, name, dbUsers} = getState();
    return this._filter(patchState, name, surname, dbUsers, condition);
  }

  @Action(UsersFilterName)
  filterName({patchState, getState}: StateContext<AuthStateModel>, {name, condition}: UsersFilterName): Observable<User[]> {
    const {surname, dbUsers} = getState();
    patchState({loading: true, name});
    return this._filter(patchState, name, surname, dbUsers, condition);
  }

  @Action(UsersFilterSurname)
  filterSurname({patchState, getState}: StateContext<AuthStateModel>, {surname, condition}: UsersFilterSurname): Observable<User[]> {
    const {name, dbUsers} = getState();
    patchState({loading: true, surname});
    return this._filter(patchState, name, surname, dbUsers, condition);
  }

  private _filter(patchState, name: string, surname: string, dbUsers: User[], condition): Observable<User[]> {
    const users = Object.assign([], dbUsers.filter((v) => {
      if (condition === 'OR') {
        return v.name.toLowerCase().includes(name.toLowerCase()) || v.surname.toLowerCase().includes(surname.toLowerCase());
      } else {
        return v.name.toLowerCase().includes(name.toLowerCase()) && v.surname.toLowerCase().includes(surname.toLowerCase());
      }
    }));
    patchState({
      users
    });
    patchState({loading: false});
    return of(users);
  }
}

