import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersComponent} from './users.component';
import {HttpClientModule} from "@angular/common/http";
import {SharedModule} from "../@shared/shared.module";
import {
  NbButtonModule,
  NbCardModule,
  NbLayoutModule,
  NbListModule, NbRadioModule,
  NbSpinnerModule,
  NbUserModule
} from "@nebular/theme";
import {NgxsModule} from "@ngxs/store";
import {RouterModule} from "@angular/router";
import {USERS_ROUTES} from "./users.routing";
import {UsersState} from "./users.state";
import {UsersService} from "./services/users.service";

@NgModule({
  declarations: [UsersComponent],
  providers: [UsersService],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,

    NbSpinnerModule,
    NbCardModule,
    NbLayoutModule,
    NbButtonModule,
    NbListModule,

    NgxsModule.forFeature([UsersState]),
    RouterModule.forChild(USERS_ROUTES),
    NbUserModule,
    NbRadioModule
  ]
})
export class UsersModule {
}
