import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AppSettings} from "../../app.settings";
import {UserResponse} from "../models/user-response";
import {map} from "rxjs/operators";
import {User} from "../models/user";

@Injectable()
export class UsersService {

  constructor(private _httpClient: HttpClient) {
  }

  public getUsers(): Observable<User[]> {
    return this._httpClient.get<UserResponse>(`${AppSettings.API_ENDPOINT}users`, {
      withCredentials: true
    })
      .pipe(
        map(value => value.data)
      );
  }

}

