import {Routes} from '@angular/router';

export const APP_ROUTES: Routes = [{
  path: '',
  loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
}, {
  path: 'login',
  loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
}, {
  path: '**',
  redirectTo: ''
}];
