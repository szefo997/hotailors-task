import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {InputValidatorDirective} from './directives/input-validator.directive';
import {ToasterModule} from 'angular2-toaster';
import {AuthGuardService} from './services/auth-guard.service';
import {ControlMessagesComponent} from './components/control-messages/control-messages.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthenticationService} from "./services/authentication.service";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,

    ToasterModule.forChild(),
  ],
  declarations: [
    ControlMessagesComponent,
    InputValidatorDirective
  ],
  providers: [
    AuthGuardService,
    AuthenticationService
  ],
  exports: [
    ControlMessagesComponent,
    InputValidatorDirective,
  ],
  entryComponents: []
})
export class SharedModule {

  constructor() {
  }

}
