import {Component, DoCheck, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {BehaviorSubject, Subscription} from 'rxjs';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'rb-control-messages',
  template: `<p class="mt-2 error-message text-danger" [@enterAnimation]
                *ngIf="touched && (errorMessage$ | async)">{{errorMessage$ | async}}</p>`,
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(-100%)', opacity: 0}),
          animate('150ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('150ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    )
  ]
})
export class ControlMessagesComponent implements OnInit, OnDestroy, DoCheck {

  @Input() control: FormControl;
  public touched = false;
  private _subscription: Subscription;
  private _errorMsg = new BehaviorSubject<string>('');
  public errorMessage$ = this._errorMsg.asObservable();

  constructor() {
  }

  ngOnInit(): void {
    this._subscription = this.control.valueChanges.subscribe(() => {
      this._setErrorMessage();
    });
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  ngDoCheck(): void {
    if (this.touched !== this.control.touched) {
      this.touched = this.control.touched;
      this._setErrorMessage();
    } else if (this.control.asyncValidator && this.control.errors && this.control.errors['isTaken']) {
      this._setErrorMessage();
    }
  }

  private _setErrorMessage(): void {
    let exists = false;
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName)) {
        exists = true;
        this._errorMsg.next(this._getValidatorErrorMessage(propertyName, this.control.errors[propertyName]));
      }
    }
    if (!exists) {
      this._errorMsg.next('');
    }
  }

  private _getValidatorErrorMessage(validatorName: string, validatorValue?: any): string {
    const config = {
      'required': 'Field is required',
    };
    return config[validatorName];
  }

}
