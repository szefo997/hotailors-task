import {Directive, DoCheck, ElementRef, HostListener, Input, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Directive({
  selector: '[rbInputValidator]'
})
export class InputValidatorDirective implements OnInit, OnDestroy, DoCheck {

  @Input('rbInputValidator') public control: FormControl;

  private _valCssClassSubj = new Subject<string>();
  private _valCssClassSubs: Subscription;

  private _floatLabelSubj = new Subject<string>();
  private _floatLabelSubs: Subscription;

  private _touched = false;

  constructor(private _renderer2: Renderer2, private _elementRef: ElementRef) {
    this._initSubscriptions();
  }

  ngOnInit(): void {
    this._floatLabelSubj.next(this.control.value);
    const validator = this.control.validator(this.control);
    if (validator && validator.required) {
      this._renderer2.addClass(this._elementRef.nativeElement.parentNode, 'rb-required');
    }
  }

  ngDoCheck(): void {
    if (this._touched !== this.control.touched) {
      this._touched = this.control.touched;
      this._valCssClassSubj.next(this.control.value);
    }
  }

  ngOnDestroy(): void {
    this._valCssClassSubs.unsubscribe();
    this._floatLabelSubs.unsubscribe();
  }

  @HostListener('keyup', ['$event.target'])
  onKeyup() {
    this._valCssClassSubj.next(this.control.value);
  }

  @HostListener('focus', ['$event.target'])
  onFocus() {
    this._floatLabelSubj.next(this.control.value);
    // todo (fix for focus out - ExpressionChangedAfterItHasBeenCheckedError)
    this.control.markAsTouched();
  }

  @HostListener('focusout', ['$event.target'])
  onFocusout() {
    this._floatLabelSubj.next(this.control.value);
    this._valCssClassSubj.next(this.control.value);
  }

  private _initSubscriptions(): void {
    this._valCssClassSubs = this._valCssClassSubj
      .pipe(
        debounceTime(250),
        distinctUntilChanged()
      )
      .subscribe((v) => {
        if (!this.control.touched && !this.control.dirty) {
          return;
        }
        if (this.control.valid) {
          this._renderer2.addClass(this._elementRef.nativeElement, 'form-control-success');
          this._renderer2.removeClass(this._elementRef.nativeElement, 'form-control-danger');
        } else {
          this._renderer2.addClass(this._elementRef.nativeElement, 'form-control-danger');
          this._renderer2.removeClass(this._elementRef.nativeElement, 'form-control-success');
        }
      });

    this._floatLabelSubs = this._floatLabelSubj
      .pipe(
        debounceTime(10),
        distinctUntilChanged()
      )
      .subscribe((val) => {
        const {value} = this.control;
        if (value !== null && value !== undefined && value !== '') {
          this._renderer2.addClass(this._elementRef.nativeElement, 'used');
        } else {
          this._renderer2.removeClass(this._elementRef.nativeElement, 'used');
        }
      });
  }


}
