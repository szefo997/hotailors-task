import {Directive, DoCheck, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Directive({
  selector: '[rbInputUniqueValidator]'
})
export class InputUniqueValidatorDirective implements OnInit, OnDestroy, DoCheck {

  @Input('rbInputUniqueValidator') public control: FormControl;
  @Output('change') change = new EventEmitter<null>();
  private _uniqueSubj = new Subject<any>();
  private _uniqueSubs: Subscription;
  private _dbValue: any;

  constructor() {
  }

  ngDoCheck(): void {
    if (this._dbValue !== this.control.value && this.control.valid) {
      this._uniqueSubj.next(this.control.value);
    }
  }

  ngOnDestroy(): void {
    if (this._uniqueSubs) {
      this._uniqueSubs.unsubscribe();
    }
  }

  ngOnInit(): void {
    this._dbValue = this.control.value;
    this._uniqueSubs = this._uniqueSubj
      .pipe(
        debounceTime(350),
        distinctUntilChanged()
      )
      .subscribe((v) => {
        console.error('subscribe v', v);
      });
  }


}
