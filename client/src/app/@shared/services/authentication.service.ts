import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AppSettings} from "../../app.settings";

@Injectable()
export class AuthenticationService {

  constructor(private _httpClient: HttpClient) {
  }

  public login(username: string, password: string): Observable<HttpResponse<Object>> {
    return this._httpClient.post<HttpResponse<Object>>(`${AppSettings.API_ENDPOINT}login`, {
      username,
      password
    }, { withCredentials: true});
  }

}

