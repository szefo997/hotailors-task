import {Injectable} from '@angular/core';
import {CanLoad, Route, Router, UrlSegment} from '@angular/router';
import {Observable, of} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class AuthGuardService implements CanLoad {

  constructor(
    private _router: Router,
    private _authenticationService: AuthenticationService
  ) {
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this._authenticationService.login('', '')
      .pipe(
        map(value => value.status === 200)
      )
      .pipe(
        tap(x => {
          if (!x) {
            this._goToLoginPage();
          }
        })
      );
  }

  private _goToLoginPage(): Observable<boolean> {
    return of(false)
      .pipe(
        tap(() => {
          this._router.navigate(['login']);
        })
      );
  }

}
