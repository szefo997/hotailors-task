import { Component } from '@angular/core';
import {ToasterConfig} from "angular2-toaster";

@Component({
  selector: 'rb-root',
  template: `
      <toaster-container [toasterconfig]="config"></toaster-container>
      <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public readonly config: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-left',
    showCloseButton: false,
    tapToDismiss: true,
    animation: 'flyLeft',
    timeout: 7000,
    limit: 10
  });

  title = 'front';
}
