import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Select, Store} from '@ngxs/store';
import {AuthInit, AuthLogin, LoginState} from './login.state';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'rb-login',
  template: `
      <nb-card [nbSpinner]="loading$ | async" nbSpinnerStatus="danger">
          <nb-card-body>

              <form (ngSubmit)="login()" *ngIf="formGroup" [formGroup]="formGroup" novalidate>

                  <h1 class="title" id="title">Log in</h1>

                  <div class="group mb-2">
                      <label class="label" for="username">User name</label>
                      <input class="input-full-width size-medium shape-rectangle"
                             formControlName="username"
                             [rbInputValidator]="f['username']"
                             id="username"
                             name="username"
                             nbInput
                             type="text">
                      <rb-control-messages [control]="f['username']"></rb-control-messages>
                  </div>

                  <div class="group">
                      <label class="label" for="password">Password</label>
                      <input
                              class="input-full-width size-medium"
                              [rbInputValidator]="f['password']"
                              formControlName="password"
                              id="password"
                              name="password"
                              nbInput
                              type="password">

                      <rb-control-messages [control]="f['password']"></rb-control-messages>
                  </div>

                  <button [disabled]="f['password'].value || !f['password'].value || (loading$ | async)"
                          class="btn btn-success btn-semi-round btn-block mt-4 mb-4"
                          nbButton>
                      Sign in
                  </button>

              </form>

          </nb-card-body>
      </nb-card>

  `,
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public formGroup: FormGroup;

  @Select(LoginState.formGroup) public formGroup$: Observable<FormGroup>;
  @Select(LoginState.loading) public loading$: Observable<boolean>;

  private _formGroupSub: Subscription;

  constructor(
    private _store: Store
  ) {
  }

  get f() {
    return this.formGroup.controls;
  }

  ngOnInit() {
    this._store.dispatch(new AuthInit());
    this._formGroupSub = this.formGroup$.subscribe(value => {
      this.formGroup = value;
    });
  }

  ngOnDestroy(): void {
    this._formGroupSub.unsubscribe();
  }

  public login(): void {
    this._store.dispatch(new AuthLogin());
  }

}
