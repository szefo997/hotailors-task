import {NgModule} from '@angular/core';

import {RouterModule} from "@angular/router";
import {LoginComponent} from "./login.component";
import {LOGIN_ROUTES} from "./login.routing";
import {NbButtonModule, NbCardModule, NbLayoutModule, NbSpinnerModule} from "@nebular/theme";
import {ReactiveFormsModule} from "@angular/forms";
import {NgxsModule} from "@ngxs/store";
import {LoginState} from "./login.state";
import {NbAuthModule} from "@nebular/auth";
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {SharedModule} from "../@shared/shared.module";

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    SharedModule,

    NbSpinnerModule,
    NbCardModule,
    NbAuthModule,
    NbLayoutModule,
    NbButtonModule,

    NgxsModule.forFeature([LoginState]),
    RouterModule.forChild(LOGIN_ROUTES),
  ],
  providers: []
})
export class LoginModule {
}
