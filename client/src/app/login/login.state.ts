import {Action, Selector, State, StateContext} from '@ngxs/store';
import {catchError, finalize, tap} from 'rxjs/operators';
import {Observable, of, throwError} from 'rxjs';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from "angular2-toaster";
import {Router} from "@angular/router";
import {NgZone} from "@angular/core";
import {AuthenticationService} from "../@shared/services/authentication.service";

export class AuthInit {
  static readonly type = '[Auth] init';

  constructor() {
  }
}

export class AuthLogin {
  static readonly type = '[Auth] login';

  constructor() {
  }
}

export interface AuthStateModel {
  formGroup: FormGroup;
  username: string;
  loading: boolean;
}

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    formGroup: null,
    username: null,
    loading: false,
  }
})
export class LoginState {

  constructor(
    private _authenticationService: AuthenticationService,
    private _toasterService: ToasterService,
    private _formBuilder: FormBuilder,
    private _ngZone: NgZone,
    private _router: Router
  ) {
  }

  @Selector()
  static loading(state: AuthStateModel) {
    return state.loading;
  }

  @Selector()
  static userName(state: AuthStateModel) {
    return state.username;
  }

  @Selector()
  static formGroup(state: AuthStateModel): FormGroup {
    return state.formGroup;
  }

  @Action(AuthInit)
  init({patchState}: StateContext<AuthStateModel>): void {
    patchState({
      formGroup: this._formBuilder.group({
        'username': new FormControl(null, [Validators.required]),
        'password': new FormControl(null, [Validators.required])
      })
    });
  }

  @Action(AuthLogin)
  login({patchState, getState}: StateContext<AuthStateModel>): Observable<any> {
    const {formGroup} = getState();
    const {username, password} = formGroup.value;

    if (!formGroup.valid) {
      return of(null);
    }

    patchState({
      username: null,
      loading: true
    });

    return this._authenticationService
      .login(username, password)
      .pipe(
        tap(() => {
          this._toasterService.pop({
            type: 'success',
            body: 'You have successfully logged in to the application'
          });
          patchState({
            username
          });
          this._navigateToUsers();
        }),
        catchError(err => {
          if (err.status === 400) {
            this._toasterService.pop({
              type: 'error',
              body: 'Bad request'
            });
          } else if (err.status === 404) {
            this._toasterService.pop({
              type: 'error',
              body: 'User not found'
            });
          }
          return throwError(err);
        }),
        finalize(() => {
          patchState({loading: false});
        })
      );
  }

  private _navigateToUsers(): void {
    this._ngZone.run(() => {
      this._router.navigateByUrl('/');
    });
  }

}

