import {Routes} from "@angular/router";
import {LoginComponent} from "./login.component";
import {NbAuthComponent} from "@nebular/auth";

export const LOGIN_ROUTES: Routes = [{
  path: '',
  component: NbAuthComponent,
  children: [{
    path: '',
    component: LoginComponent
  }]
}];
