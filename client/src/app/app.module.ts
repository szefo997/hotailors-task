import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule} from "@angular/router";
import {APP_ROUTES} from "./app.routing";
import {NgxsModule} from "@ngxs/store";
import {NgxsRouterPluginModule} from "@ngxs/router-plugin";
import {NgxsStoragePluginModule} from "@ngxs/storage-plugin";
import {NbThemeModule} from "@nebular/theme";
import {NbAuthModule} from "@nebular/auth";
import {HttpClientModule} from "@angular/common/http";
import {NbEvaIconsModule} from "@nebular/eva-icons";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CookieService} from "angular2-cookie/core";
import {ToasterModule} from "angular2-toaster";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,

    RouterModule.forRoot(
      APP_ROUTES, {enableTracing: false}
    ),

    NgxsModule.forRoot([], {
      developmentMode: false
    }),

    NgxsRouterPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot({
      key: []
    }),

    NbEvaIconsModule,
    NbThemeModule.forRoot(),
    NbAuthModule.forRoot(),
    ToasterModule.forRoot()

  ],

  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
